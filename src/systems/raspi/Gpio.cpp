#include "System.h"
#include "systems/raspi/Gpio.h"

using sys::arch::get32;
using sys::arch::put32;
using sys::arch::delay;

namespace systems::raspi::gpio {

Gpio::Gpio() {}

void Gpio::DisablePupPdn(u32 pins) {
	if constexpr(sys::Model <= systems::raspi::Model::raspi4) {
		put32(GPIO_BASE + O_GPPUD, 0);
		delay(150);
		put32(GPIO_BASE + O_GPPUDCLK[0], pins);
		delay(150);
		//put32(GPIO_BASE + O_GPPUD, 0); // says to do this in manual?
		put32(GPIO_BASE + O_GPPUDCLK[0], 0);
	} else if constexpr(sys::Model == systems::raspi::Model::raspi4) {
		static_assert(sys::Model != systems::raspi::Model::raspi4);
	} else {
		static_assert(sys::Model < systems::raspi::Model::COUNT);
	}
}

}
