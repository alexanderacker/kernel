#include "System.h"
#include "systems/raspi/Uart.h"
#include "systems/raspi/Gpio.h"

using sys::arch::get32;
using sys::arch::put32;

namespace systems::raspi::uart {

Uart::Uart()
{
	systems::raspi::gpio::Gpio g{};

	// disable uart
	put32(UART0_BASE + O_CR, 0x0);
	
	g.DisablePupPdn((u32)((1 << 14) | (1 << 15)));
	
	// Uart Setup
	put32(UART0_BASE + O_ICR, 0x7ff);
	put32(UART0_BASE + O_IBRD, 1);
	put32(UART0_BASE + O_FBRD, 40);
	put32(UART0_BASE + O_LCRH, LCRH_FEN | LCRH_WLEN);
	put32(UART0_BASE + O_IMSC, IMSC_CTSMIM | IMSC_RXIM | IMSC_TXIM | IMSC_RTIM | IMSC_FEIM | IMSC_PEIM | IMSC_BEIM | IMSC_OEIM);
	put32(UART0_BASE + O_CR, CR_UARTEN | CR_TXE | CR_RXE);
	
	/*
	MaskReg(UART0_BASE + O_LCRH, LCRH_PEN, false);
	MaskReg(UART0_BASE + O_LCRH, LCRH_STP2, false);
	MaskReg(UART0_BASE + O_LCRH, LCRH_FEN, false);
	// LCRH_WLEN should be 0b11
	MaskReg(UART0_BASE + O_LCRH, LCRH_SPS, false);

	// TODO: pretty sure enable comes first after GPIO config
	MaskReg(UART0_BASE + O_CR, CR_UARTEN, true);
	*/
}

void
Uart::put
(u8 byte)
{
	while(get32(UART0_BASE + O_FR) & FR_TXFF) {
	
	}

	put32(UART0_BASE + O_DR, byte);
}

u8
Uart::get
()
{
	while(get32(UART0_BASE + O_FR) & FR_RXFE) {

	}
	return (u8)(get32(UART0_BASE + O_DR) & DR_DATA);
}

}


