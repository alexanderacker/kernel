#include "Assert.h"
#include "System.h"
#include "systems/raspi/Aux.h"
#include "systems/raspi/Gpio.h"

using sys::arch::get32;
using sys::arch::put32;

namespace systems::raspi::aux {

Miniuart::Miniuart()
{
	systems::raspi::gpio::Gpio g{};	
	// Setup GPIO pin config
	u32 select = get32(GPIO_BASE + systems::raspi::gpio::O_GPFSEL[1]);
	select &= ~(7 << 12);
	select |= 2 << 12;
	select &= ~(7 << 15);
	select |= 2 << 15;
	put32(GPIO_BASE + systems::raspi::gpio::O_GPFSEL[1], select);

	g.DisablePupPdn((u32)((1 << 14) | (1 << 15)));
	
	// Setup MiniUart
	put32(AUX_BASE + O_ENABLES, ENABLES_MU);
	put32(AUX_BASE + O_MU_CNTL, 0);
	put32(AUX_BASE + O_MU_IER, 0);
	put32(AUX_BASE + O_MU_LCR, MU_LCR_DATASIZE);
	put32(AUX_BASE + O_MU_MCR, 0);
	put32(AUX_BASE + O_MU_BAUD, 270);
	put32(AUX_BASE + O_MU_CNTL, MU_CNTL_ENABLE_RECEIVE & MU_CNTL_ENABLE_TRANSMIT);
}

void
Miniuart::put
(u8 byte)
{
	while (1) {
		if (get32(AUX_BASE + O_MU_LSR) & MU_LSR_EMPTY)
			break;
	}
	put32(AUX_BASE + O_MU_IO, byte);
}

u8
Miniuart::get
()
{
	while(1) {
		if (get32(AUX_BASE + O_MU_LSR) & MU_LSR_READY)
			break;
	}
	return (u8)(get32(AUX_BASE + O_MU_IO) & MU_IO_DATA);
}

}


