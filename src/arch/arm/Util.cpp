#include "arch/arm/Util.h"

namespace arch::arm {

u32 get32(u32 addr) {
	u32 ret;
	asm volatile("ldr %0, [%1]" : "=r" (ret) : "Ir" (addr));
	return ret;
}

void put32(u32 addr, u32 word) {
	asm volatile("str %0, [%1]" :  : "Ir" (word), "Ir" (addr));
	return;
}

void delay(u32 ticks) {
	asm volatile(R"(
	wait:
		subs %0, %0, #1
		bne wait
	)" : "=r" (ticks));
	return;
}

u8 corenumber() {
	u8 ret;
	asm volatile(R"(
		mrc p15, #0, %0, c0, c0, #5
		and %0, %0, #3
	)" : "=r" (ret));
	return ret;
}


}
