.section ".text.boot"

.equ initial_stack, 16384
.equ singlecore, 0x410FB767

/* r0, r1, r2, r15 have initial values */
.global boot
boot:
	/* Save the special atags for later */
	mov r5, r2

	/* Setup the stack to be end of code plus an offset */
	ldr sp, =__end
	mov r4, #initial_stack
	add sp, sp, r4

	/* CPU 0 goes to firstcore, the rest to busywait */
	bl get_cpu_id
	mov r6, r0
	cmp r0, #0
	beq firstcore
busywait:
	ldr r0, =core_finished
	bl primitive_waiton
	b enter_kernel
firstcore:
	/* Clear the bss section */
	ldr r0, =__bss_start
	ldr r1, =__bss_end
	bl clear_region

	/* Tell the busywaiters that we are finished */
	ldr r3, =core_finished
	mov r4, #1
	str r4, [r3, #0]
enter_kernel:
	/* Enter the kernel */
	mov r0, r6 /* cpu number back in r0 */
	mov r1, r5 /* atags back in r1 */
	bl kmain
	b primitive_halt

/* Value that lets other cores know when they can enter the kernel */
core_finished: .word 0

/* Gets the core id number
   IN: void
   OUT: r0 = core id       */
get_cpu_id:
	ldr r2, =singlecore
	mrc p15, #0, r0, c0, c0, #5
	cmp r0, r2
	beq get_cpu_id_singlecore
	and r0, r0, #3
	mov pc, lr
get_cpu_id_singlecore:
	and r0, r0, #0
	mov pc, lr


/*  Sets all values to zero in a region
    IN: r0 = start address, r1 = end address
    OUT: void                                
Note: Could store more at once but its unnecessary */
clear_region:
	mov r2, #0
clear_region_loop:
	stmia r0!, {r2}
	cmp r0, r1
	blo clear_region_loop
	mov pc, lr

/*  Waits for a value to become non-zero (synchronization)
    IN: r0 = address of the value to wait for
    OUT: void                                              */
primitive_waiton:
	ldr r1, [r0, #0]
	cmp r1, #0
	beq primitive_waiton
	mov pc, lr

/* Loops the cpu forever */
primitive_halt:
	wfe
	b primitive_halt
