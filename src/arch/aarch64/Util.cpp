#include "arch/aarch64/Util.h"

namespace arch::aarch64 {

u32 get32(u32 addr) {
	u32 ret;
	asm volatile("ldr %w0, [%1]" : "=r" (ret) : "Ir" (addr));
	return ret;
}

void put32(u32 addr, u32 word) {
	asm volatile("str %w0, [%1]" :  : "Ir" (word), "Ir" (addr));
	return;
}

void delay(u32 ticks) {
	asm volatile(R"(
	wait:
		subs %w0, %w0, #1
		bne wait
	)" : "=r" (ticks));
	return;
}

u8 corenumber() {
	u8 ret;
	asm volatile(R"(
		mrs %0, MPIDR_EL1
		and %0, %0, #3
	)" : "=r" (ret));
	return ret;
}

}
