.section ".text.boot"

.equ initial_stack, 16384

/* x0, x1, x2, x15 have initial values */
.global boot
boot:
	/* Save the special atags for later */
	mov x5, x2

	/* Setup the stack to be end of code plus an offset */
	ldr x4, =__end
	mov sp, x4
	mov x4, #initial_stack
	add sp, sp, x4

	/* CPU 0 goes to firstcore, the rest to busywait */
	bl get_cpu_id
	mov x6, x0
	cmp x0, #0
	beq firstcore
busywait:
	ldr x0, =core_finished
	bl primitive_waiton
	b enter_kernel
firstcore:
	/* Clear the bss section */
	ldr x0, =__bss_start
	ldr x1, =__bss_end
	bl clear_region

	/* Tell the busywaiters that we are finished */
	ldr x3, =core_finished
	mov x4, #1
	str x4, [x3, #0]
enter_kernel:
	/* Enter the kernel */
	mov x0, x6 /* cpu number back in x0 */
	mov x1, x5 /* atags back in x1 */
	bl kmain
	b primitive_halt

/* Value that lets other cores know when they can enter the kernel */
core_finished: .word 0

/* Gets the core id number
   IN: void
   OUT: x0 = core id       */
get_cpu_id:
	//mrc p15, #0, x0, c0, c0, #5 /* REPLACE with msr? */
	mrs x0, MPIDR_EL1
	and x0, x0, #3
	ret x30


/*  Sets all values to zero in a xegion
    IN: x0 = start address, x1 = end address
    OUT: void                                
Note: Could store more at once but its unnecessary */
clear_region:
	mov x2, #0
clear_region_loop:
	//stmia x0!, {x2}
	strh w2, [x0], #4
	cmp x0, x1
	blo clear_region_loop
	ret x30

/*  Waits for a value to become non-zero (synchronization)
    IN: x0 = address of the value to wait for
    OUT: void                                              */
primitive_waiton:
	ldr x1, [x0, #0]
	cmp x1, #0
	beq primitive_waiton
	ret x30

/* Loops the cpu forever */
primitive_halt:
	wfe
	b primitive_halt
