#include "Type.h"
#include "System.h"
#include "kernel/Main.h"
#include "device/Uart.h"

using sys::arch::corenumber;

namespace kernel {

extern "C" void kmain()
{	
	if (sys::hardware::Multicore && corenumber() > 0) {
		while(true) {}
		return;
	}
	sys::hardware::DebugUart uart{};
	//something(uart);
	uart.put('h');
	uart.put('e');
	uart.put('l');
	uart.put('l');
	uart.put('o');
	uart.put('\n');
	while (true) {
		u8 res = uart.get();
		uart.put(res);
	}
	return;
}

}
