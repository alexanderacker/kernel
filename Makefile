.DEFAULT_GOAL := all

include mk/setup.mk

.PHONY: all clean distclean dirs clean-dirs run run-gdb image
all: setup-check dirs $(TARGETLOC)

ifdef QEMU_NAME
run: all
	qemu-system-$(ARCH) -M $(QEMU_NAME) -m $(SYSTEM_MEM) -nographic -kernel $(TARGETLOC)

run-gdb: all
	qemu-system-$(ARCH) -M $(QEMU_NAME) -m $(SYSTEM_MEM) -serial stdio -kernel $(TARGETLOC) -s -S
endif

clean:
	$(RM) $(OBJECTS) $(ASM_OBJECTS)

distclean: clean-dirs

dirs:
	@$(MKDIR) $(BUILDDIR) $(OBJECTDIR) $(OBJECTDIRS)

clean-dirs:
	$(RM_RECURSE) $(BUILDDIR) $(OBJECTDIR) kernel*.img

$(OBJECTS):
	$(ECHO) "C++	$@\n"
	@$(CXX) $(CXXFLAGS) $(CXXDYNINC) $(CXXWARN) $(OPTLEVEL) $(INCLUDE) -MMD -o $@ -c $(patsubst $(OBJECTDIR)/%.$(OBJECTEXT),$(SOURCEDIR)/%.$(SOURCEEXT),$@)

$(ASM_OBJECTS):
	$(ECHO) "ASM	$@\n"
	@$(AS) $(ASFLAGS) -o $@ $(patsubst $(OBJECTDIR)/%.$(OBJECTEXT),$(SOURCEDIR)/%.$(ASMEXT),$@)

$(TARGETLOC): $(ASM_OBJECTS) $(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $(ASM_OBJECTS) $(OBJECTS) $(LIBRARY)

image: all
	$(OBJCOPY) $(TARGETLOC) -O binary $(SYSTEM_IMAGE) 

-include $(DEPENDS)
