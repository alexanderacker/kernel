#ifndef SYSTEM_H
#define SYSTEM_H

#define _MACROTOSTR(x) #x
#define MACROTOSTR(x) _MACROTOSTR(x)

#include MACROTOSTR(KERNEL_INCLUDE_ARCH)
#include MACROTOSTR(KERNEL_INCLUDE_SYSTEM)

namespace sys {
	namespace arch = arch::KERNEL_ARCH; 
	namespace hardware = systems::KERNEL_SYSTEM;

	constexpr hardware::Model Model = hardware::Model::KERNEL_SYSTEM_MODEL;

	namespace strings {
		constexpr str Arch   = MACROTOSTR(KERNEL_ARCH);
		constexpr str System = MACROTOSTR(KERNEL_SYSTEM);
		constexpr str Model  = MACROTOSTR(KERNEL_SYSTEM_MODEL);
	}
}

#undef _MACROTOSTR
#undef MACROTOSTR

#endif
