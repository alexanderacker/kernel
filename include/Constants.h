#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <initializer_list>
#include <tuple>

namespace constants {

/* This is implementation dependent:
 * GCC and Clang have this macro
 * It will need to be changed for others */
consteval void Unreachable() {
	__builtin_unreachable();
}

/* Compile time constant mapping:
 *     For a typename: A and typename: B
 *     For a mapping: init_list of ((init_list of A), B)
 *     Maps 'value' of A to one of B using the mapping given
 *     Fails at compile-time if 'value' is not in any mapping
 *     ie: many A's can map to the same B
 * See include/systems/raspi/System.h PERIPHERAL_BASE for an example
 * WARNING: Values of type A should only be used once, otherwise
 *     the mapping will use the first one defined. */
template<typename A, typename B, A value>
consteval B Map(std::initializer_list<std::tuple<std::initializer_list<A>, B>> mappings) {
	for (auto t: mappings) {
		const auto map = std::get<0>(t);
		for (auto a: map) {
			if (a == value) {
				return std::get<1>(t);
			}
		}
	}
	Unreachable();
}

}

#endif
