#ifndef KERNEL_MAIN
#define KERNEL_MAIN

#include "Type.h"

namespace kernel {

extern "C" void kmain();

}

#endif
