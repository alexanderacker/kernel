#ifndef RASPI_MODEL_H
#define RASPI_MODEL_H

namespace systems::raspi {

enum Model : u32 {
	raspi0,
	raspi1,
	raspi2,
	raspi3,
	raspi4,
	COUNT
};


}

#endif
