#ifndef RASPI_AUX_H
#define RASPI_AUX_H

#include "Type.h"
#include "Constants.h"
#include "systems/raspi/System.h"

namespace systems::raspi::aux {

struct Miniuart {
	Miniuart();
	void put(u8 byte);
	u8 get();
};

/* BCM2835 ARM Peripherals Manual
 * Chapter 2 - Auxiliaries UART1 SPI1 & SPI2 */

/* Auxiliary Interrupt status */
constexpr u32 O_IRQ = 0x0;
	constexpr u32 IRQ_MU   = (1 << 0);
	constexpr u32 IRQ_SPI1 = (1 << 1);
	constexpr u32 IRQ_SPI2 = (1 << 2);

/* Auxiliary Enables */
constexpr u32 O_ENABLES = 0x04;
	constexpr u32 ENABLES_MU   = (1 << 0);
	constexpr u32 ENABLES_SPI1 = (1 << 1);
	constexpr u32 ENABLES_SPI2 = (1 << 2);

/* MINI UART */

/* I/O Data */
constexpr u32 O_MU_IO = 0x40;
	constexpr u32 MU_IO_DATA = 0xff;

/* Interrupt Enable */
constexpr u32 O_MU_IER = 0x44;
	constexpr u32 MU_IER_ENABLETRANSMIT = (1 << 0);
	constexpr u32 MU_IER_ENABLERECEIVE  = (1 << 1);

/* Interrupt Identify */
constexpr u32 O_MU_IIR = 0x48;
	constexpr u32 MU_IIR_PENDING         = (1 << 0);
	constexpr u32 MU_IIR_READ_ID         = (0b11 << 1);
	constexpr u32 MU_IIR_WRITE_FIFOCLEAR = (0b11 << 1);
	constexpr u32 MU_IIR_FIFO_ENABLE     = (0b11 << 6);

/* Line Control */
constexpr u32 O_MU_LCR = 0x4c;
	constexpr u32 MU_LCR_DATASIZE = 0b11;
	constexpr u32 MU_LCR_BREAK    = (1 << 6);
	constexpr u32 MU_LCR_DLAB     = (1 << 7);

/* Modem Control */
constexpr u32 O_MU_MCR = 0x50;
	constexpr u32 MU_MCR_RTS = (1 << 1);

/* Line Status */
constexpr u32 O_MU_LSR = 0x54;
	constexpr u32 MU_LSR_READY   = (1 << 0);
	constexpr u32 MU_LSR_OVERRUN = (1 << 1);
	constexpr u32 MU_LSR_EMPTY   = (1 << 5);
	constexpr u32 MU_LSR_IDLE    = (1 << 6);

/* Modem Status */
constexpr u32 O_MU_MSR = 0x58;
	constexpr u32 MU_MSR_CTS_STATUS =
		constants::Map<Model, u32, Model::KERNEL_SYSTEM_MODEL>({
			{{raspi0, raspi1, raspi2, raspi3}, (1 << 5)},
			{{raspi4}, (1 << 4)}
		});

/* Scratch */
constexpr u32 O_MU_SCRATCH = 0x5c;
	constexpr u32 MU_SCRATCH_DATA = 0xff;

/* Extra Control */
constexpr u32 O_MU_CNTL = 0x60;
	constexpr u32 MU_CNTL_ENABLE_RECEIVE    = (1 << 0);
	constexpr u32 MU_CNTL_ENABLE_TRANSMIT   = (1 << 1);
	constexpr u32 MU_CNTL_RTS_AUTOFLOWCNTL  = (1 << 2);
	constexpr u32 MU_CNTL_CTS_AUTOFLOWCNTL  = (1 << 3);
	constexpr u32 MU_CNTL_RTS_AUTOFLOWLEVEL = (0b11 << 4);
	constexpr u32 MU_CNTL_RTS_ASSERTLEVEL   = (1 << 6);
	constexpr u32 MU_CNTL_CTS_ASSERTLEVEL   = (1 << 7);

/* Extra Status */
constexpr u32 O_MU_STAT = 0x64;
	constexpr u32 MU_STAT_SYMBOLAVAILABLE  = (1 << 0);
	constexpr u32 MU_STAT_SPACEAVAILABLE   = (1 << 1);
	constexpr u32 MU_STAT_RECEIVE_IDLE     = (1 << 2);
	constexpr u32 MU_STAT_TRANSMIT_IDLE    = (1 << 3);
	constexpr u32 MU_STAT_RECEIVE_OVERRUN  = (1 << 4);
	constexpr u32 MU_STAT_TRANSMIT_FULL    = (1 << 5);
	constexpr u32 MU_STATUS_RTS_STATUS     = (1 << 6);
	constexpr u32 MU_STATUS_CTS_STATUS     = (1 << 7);
	constexpr u32 MU_STATUS_TRAMSIT_EMPTY  = (1 << 8);
	constexpr u32 MU_STATUS_TRAMSIT_DONE   = (1 << 9);
	constexpr u32 MU_STATUS_RECEIVE_COUNT  = (0xf << 16);
	constexpr u32 MU_STATUS_TRANSMIT_COUNT = (0xf << 24);

/* Baudrate */
constexpr u32 O_MU_BAUD = 0x68;
	constexpr u32 MU_BAUD_RATE = 0xffff;

}

#endif
