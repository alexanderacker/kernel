#ifndef RASPI_LINKER_H
#define RASPI_LINKER_H

#include "Constants.h"

namespace systems::raspi::linker {

constexpr u32 LOAD_ADDRESS =
	constants::Map<Model, u32, Model::KERNEL_SYSTEM_MODEL>({
		{{raspi0, raspi1}, 0x4096},
		{{raspi2}, 0x4096},
		{{raspi3}, 0x8000},
		{{raspi4}, 0x8000}
	});

}

#endif
