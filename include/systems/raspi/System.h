#ifndef RASPI_SYSTEM_H
#define RASPI_SYSTEM_H

#include "Constants.h"
#include "systems/raspi/Model.h"
#include "systems/raspi/Aux.h"
#include "systems/raspi/Gpio.h"
#include "systems/raspi/Uart.h"

namespace systems::raspi {

constexpr bool Multicore =
	constants::Map<Model, bool, Model::KERNEL_SYSTEM_MODEL>({
		{{Model::raspi0, Model::raspi1}, false},
		{{Model::raspi2, Model::raspi3, Model::raspi4}, true}
	});

using DebugUart = uart::Uart;

constexpr u32 PERIPHERAL_BASE =
	constants::Map<Model, u32, Model::KERNEL_SYSTEM_MODEL>({
		{{Model::raspi0, Model::raspi1}, 0x20000000},
		{{Model::raspi2, Model::raspi3}, 0x3F000000},
		{{Model::raspi4}, 0x7E000000}
	});

constexpr u32 AUX_OFFSET  = 0x215000;
constexpr u32 GPIO_OFFSET = 0x200000;
constexpr u32 UART0_OFFSET = 0x201000;

constexpr u32 AUX_BASE = PERIPHERAL_BASE + AUX_OFFSET;
constexpr u32 GPIO_BASE = PERIPHERAL_BASE + GPIO_OFFSET;
constexpr u32 UART0_BASE = PERIPHERAL_BASE + UART0_OFFSET;

}

#endif
