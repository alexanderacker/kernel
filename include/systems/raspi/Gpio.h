#ifndef RASPI_GPIO_H
#define RASPI_GPIO_H

#include "Type.h"

namespace systems::raspi::gpio {

struct Gpio {
	Gpio();
	void DisablePupPdn(u32 pins);
};

/* BCM2835 ARM Peripherals Manual
 * Chapter 5 - GPIO              */

#if (SYSTEM == raspi4)
	constexpr u32 HIGHEST_PIN = 57;
#else
	constexpr u32 HIGHEST_PIN = 53;
#endif

/* Function Select Registers, define operation of GPIO pins */
constexpr u32 O_GPFSEL[6] = {0x00, 0x04, 0x08, 0x0c, 0x10, 0x14};

/* Set and Clear pins 0-31, 32-57 */
constexpr u32 O_GPSET[2] = {0x1c, 0x20};
constexpr u32 O_GPCLR[2] = {0x28, 0x2c};

/* Pin level registers return value of the pin */
constexpr u32 O_GPLEV[2] = {0x34, 0x38};

/* Event detection, record level and edge events on GPIO pins */
constexpr u32 O_GPEDS[2] = {0x40, 0x44};

/* Rising and falling edge detection (events) */
constexpr u32 O_GPREN[2] = {0x4c, 0x50};
constexpr u32 O_GPFEN[2] = {0x58, 0x5c};

/* High and low level detection (events) */
constexpr u32 O_GPHEN[2] = {0x64, 0x68};
constexpr u32 O_GPLEN[2] = {0x70, 0x74};

/* Asynchronous rising and falling edge detection (events) */
constexpr u32 O_GPAREN[2] = {0x7c, 0x80};
constexpr u32 O_GPAFEN[2] = {0x88, 0x8c};

//#if (SYSTEM == raspi4)
	/* Pull-up Pull-down control registers */
//	constexpr u32 O_GPIO_PUP_PDN_CNTRL_REG[4] = {0xe4, 0xe8, 0xec, 0xf0};
//#else
	/* Pull-up/down Enable */
	constexpr u32 O_GPPUD = 0x94;
	constexpr u32 O_GPPUDCLK[2] = {0x98, 0x9c};
//#endif

}

#endif
