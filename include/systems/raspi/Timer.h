#ifndef RASPI_SYSTEMTIMER_H
#define RASPI_SYSTEMTIMER_H

#include "Type.h"
#include "systems/raspi/Peripheral.h"

namespace systems::raspi::timer {

/* BCM2835 ARM Peripherals Manual
 * Chapter 12 - System Timer      */

constexpr u32 BASE = (PERIPHERAL_BASE + 0x3000);

/* Timer control and status register */
constexpr u32 O_CS = 0x0;
	constexpr u32 CS_M0 = (1 << 0);
	constexpr u32 CS_M1 = (1 << 1);
	constexpr u32 CS_M2 = (1 << 2);
	constexpr u32 CS_M3 = (1 << 3);

/* Timer counter lower bits */
constexpr u32 O_CLO = 0x4; // bits 31:0

/* Timer counter high bits */
constexpr u32 O_CHI = 0x8; // bits 31:0

/* Timer compare registers for channels 0 to 3 */
constexpr u32 O_C0 = 0xc;
constexpr u32 O_C1 = 0x10;
constexpr u32 O_C2 = 0x14;
constexpr u32 O_C3 = 0x18;

}

#endif
