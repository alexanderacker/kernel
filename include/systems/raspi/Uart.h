#ifndef RASPI_UART_H
#define RASPI_UART_H

#include "Type.h"

namespace systems::raspi::uart {

struct Uart {
	u8 val;
	Uart();
	void put(u8 byte);
	u8 get();
};

/* BCM2835 ARM Peripherals Manual
 * Chapter 13 - UART              */

/* Data register, also has error status bits */
constexpr u32 O_DR = 0x0;
	constexpr u32 DR_DATA = 0xff;
	constexpr u32 DR_FE   = (1 << 8);
	constexpr u32 DR_PE   = (1 << 9);
	constexpr u32 DR_BE   = (1 << 10);
	constexpr u32 DR_OE   = (1 << 11);

/* Receive status register and error clear register */
constexpr u32 O_RSRECR = 0x4;
	constexpr u32 RSRECR_FE = (1 << 0);
	constexpr u32 RSRECR_PE = (1 << 1);
	constexpr u32 RSRECR_BE = (1 << 2);
	constexpr u32 RSRECR_OE = (1 << 3);

/* Flag register */
constexpr u32 O_FR = 0x18;
	constexpr u32 FR_CTS  = (1 << 0);
	constexpr u32 FR_BUSY = (1 << 3);
	constexpr u32 FR_RXFE = (1 << 4);
	constexpr u32 FR_TXFF = (1 << 5);
	constexpr u32 FR_RXFF = (1 << 6);
	constexpr u32 FR_TXFE = (1 << 7);

/* Integer part of the baud rate divisor value */
constexpr u32 O_IBRD = 0x24; // bits 15:0 are used
	constexpr u32 IBRD_MASK = 0xffff;

/* Fractional part of the baud rate divisor value
 * BAUDDIV = (FUARTCLK/(16 * BAUDRATE)) = IBRD + FBRD
 * FUARTCLOCK = Uart reference clock frequency        */
constexpr u32 O_FBRD = 0x28; // bits 5:0 are used
	constexpr u32 FBRD_MASK = 0x1f;

/* Line control register */
constexpr u32 O_LCRH = 0x2c;
	constexpr u32 LCRH_BRK  = (1 << 0);
	constexpr u32 LCRH_PEN  = (1 << 1);
	constexpr u32 LCRH_EPS  = (1 << 2);
	constexpr u32 LCRH_STP2 = (1 << 3);
	constexpr u32 LCRH_FEN  = (1 << 4);
	constexpr u32 LCRH_WLEN = (0b11 << 5); // bits 6:5
	constexpr u32 LCRH_SPS  = (1 << 7);

/* Control register */
constexpr u32 O_CR = 0x30;
	constexpr u32 CR_UARTEN = (1 << 0);
	constexpr u32 CR_LBE    = (1 << 7);
	constexpr u32 CR_TXE    = (1 << 8);
	constexpr u32 CR_RXE    = (1 << 9);
	constexpr u32 CR_RTS    = (1 << 11);
	constexpr u32 CR_RTSEN  = (1 << 14);
	constexpr u32 CR_CTSEN  = (1 << 15);

/* Interrupt FIFO level select register */
constexpr u32 O_IFLS = 0x34;
	constexpr u32 IFLS_TXIFLSEL = (0b111 << 0); // bits 2:0
	constexpr u32 IFLS_RXIFLSEL = (0b111 << 3); // bits 5:3
	constexpr u32 IFLS_TXIFPSEL = (0b111 << 6); // etc
	constexpr u32 IFLS_RXIFPSEL = (0b111 << 9);

/* Interrupt mask set/clear register */
constexpr u32 O_IMSC = 0x38;
	constexpr u32 IMSC_CTSMIM = (1 << 1);
	constexpr u32 IMSC_RXIM   = (1 << 4);
	constexpr u32 IMSC_TXIM   = (1 << 5);
	constexpr u32 IMSC_RTIM   = (1 << 6);
	constexpr u32 IMSC_FEIM   = (1 << 7);
	constexpr u32 IMSC_PEIM   = (1 << 8);
	constexpr u32 IMSC_BEIM   = (1 << 9);
	constexpr u32 IMSC_OEIM   = (1 << 10);

/* Raw interrupt status register, Read only */
constexpr u32 O_RIS = 0x3c;
	constexpr u32 RIS_CTS = (1 << 1);
	constexpr u32 RIS_RX  = (1 << 4);
	constexpr u32 RIS_TX  = (1 << 5);
	constexpr u32 RIS_RT  = (1 << 6);
	constexpr u32 RIS_FE  = (1 << 7);
	constexpr u32 RIS_PE  = (1 << 8);
	constexpr u32 RIS_BE  = (1 << 9);
	constexpr u32 RIS_OE  = (1 << 10);

/* Masked interrupt status register */
constexpr u32 O_MIS = 0x40;
	// SAME EXACT FLAGS AS RIS

/* Interrupt clear register */
constexpr u32 O_ICR = 0x44;

/* DMA Control register, DISABLED on older? (<=pi2?) systems */
// TODO: Find a way to get rid of macro here...
//#if SYSTEM == raspi4
constexpr u32 O_DMACR = 0x48;
	constexpr u32 DMACR_RXDMAE   = (1 << 0);
	constexpr u32 DMACR_TXDMAE   = (1 << 1);
	constexpr u32 DMACR_DMAONERR = (1 << 2);
//#endif

/* Test control registers */
constexpr u32 O_ITCR = 0x80;
constexpr u32 O_ITOP = 0x88;
constexpr u32 O_TDR = 0x8c;

}

#endif
