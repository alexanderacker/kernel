#ifndef ARM_UTIL_H
#define ARM_UTIL_H

#include "Type.h"

namespace arch::arm {

u32 get32(u32 addr);
void put32(u32 addr, u32 word);
void delay(u32 ticks);
u8 corenumber();

}

#endif
