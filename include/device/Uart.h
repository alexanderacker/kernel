#ifndef DEVICE_UART
#define DEVICE_UART

#include "Type.h"
#include <concepts>

namespace device {

template<typename T>
concept Uart =
	requires (T t) {
		{ t.get() } -> std::same_as<u8>;
	} &&
	requires (T t, u8 b) {
		{ t.put(b) } -> std::same_as<void>;
	};

}
#endif
