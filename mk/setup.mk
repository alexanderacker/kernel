include config.mk
include mk/system/$(SYSTEM).mk

# BUILD INFORMATION
BUILDDIR   := bin
INCLUDEDIR := include
OBJECTDIR  := obj
SOURCEDIR  := src
SOURCEEXT  := cpp
ASMEXT     := s
DEPENDEXT  := d
OBJECTEXT  := o

TARGET     := $(SYSTEM)-$(ARCH).elf
TARGETLOC  := $(BUILDDIR)/$(TARGET)

# COMPILER INFORMATION
CC  := $(PREFIX)gcc
CXX := $(PREFIX)gcc
AS  := $(PREFIX)as
LD  := $(PREFIX)ld
CXXFLAGS  := -g -fPIC $(SYSTEM_CFLAGS) -fno-exceptions -ffunction-sections -fno-rtti -fno-common -nostdlib -I. 
CXXDYNINC := -DKERNEL_ARCH=$(ARCH) -DKERNEL_SYSTEM_MODEL=$(SYSTEM) -DKERNEL_SYSTEM=raspi -DKERNEL_INCLUDE_SYSTEM=systems/raspi/System.h -DKERNEL_INCLUDE_ARCH=arch/$(ARCH)/Arch.h
CXXWARN   := -Wall -Wextra -Wcast-align -Wcast-qual -Wconversion -Wctor-dtor-privacy -Wdisabled-optimization -Wdouble-promotion -Wmissing-declarations -Wnon-virtual-dtor -Wpointer-arith -Wredundant-decls -Wshadow -Wsign-conversion -Wswitch-default
OPTLEVEL  := -std=c++20 -O2
LDFLAGS   := -static --nmagic -T src/arch/$(ARCH)/linker.ld
ASFLAGS   := -W -g

# OTHER TOOLCHAIN
OBJDUMP := $(PREFIX)objdump
OBJCOPY := $(PREFIX)objcopy

# TERMINAL PROGRAMS
ECHO       ?= @printf
FIND       ?= find
MKDIR      ?= mkdir -p
RM         ?= rm -f
RM_RECURSE ?= $(RM) -r

INCLUDE := -I$(INCLUDEDIR)
LIBRARY :=

.PHONY: setup-check
setup-check: system-check
ifeq ($(filter $(ARCH), $(SYSTEM_ARCH)),)
	$(error Architecture $(ARCH) not compatible with $(SYSTEM) ($(SYSTEM_ARCH)))
else

SOURCEDIRS    := $(shell $(FIND) $(SOURCEDIR) -type d -not -path "$(SOURCEDIR)/arch/*") $(SOURCEDIR)/arch/$(ARCH)
OBJECTDIRS    := $(patsubst $(SOURCEDIR)%, $(OBJECTDIR)%, $(SOURCEDIRS))

INDEP_SOURCES := $(shell $(FIND) $(SOURCEDIR) -type f -name *.$(SOURCEEXT) -not -path "$(SOURCEDIR)/arch/*")
ARCH_SOURCES  := $(shell $(FIND) $(SOURCEDIR)/arch/$(ARCH) -type f -name *.$(SOURCEEXT)) 
SOURCES       := $(ARCH_SOURCES) $(INDEP_SOURCES)

OBJECTS       := $(patsubst $(SOURCEDIR)%, $(OBJECTDIR)%, $(SOURCES:.$(SOURCEEXT)=.$(OBJECTEXT)))

ASM_SOURCES   := $(shell $(FIND) $(SOURCEDIR)/arch/$(ARCH) -type f -name *.$(ASMEXT)) 
ASM_OBJECTS   := $(patsubst $(SOURCEDIR)%, $(OBJECTDIR)%, $(ASM_SOURCES:.$(ASMEXT)=.$(OBJECTEXT)))

DEPENDS       := $(OBJECTS:.$(OBJECTEXT)=.$(DEPENDEXT))

endif
