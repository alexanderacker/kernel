# Raspberry Pi 2B
SYSTEM_CPU := cortex-a7
SYSTEM_MEM := 1024

SYSTEM_CFLAGS := -mcpu=$(SYSTEM_CPU)
SYSTEM_ARCH := arm
SYSTEM_IMAGE := kernel7.img

QEMU_NAME := $(SYSTEM)

.PHONY: system-check
system-check:
