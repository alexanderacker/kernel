# Raspberry Pi 3B
SYSTEM_CPU := cortex-a53
SYSTEM_MEM := 1024

SYSTEM_CFLAGS := -mcpu=$(SYSTEM_CPU)
SYSTEM_ARCH := arm aarch64

QEMU_NAME := $(SYSTEM)

.PHONY: system-check
system-check:
ifeq ("$(ARCH)", "arm")
SYSTEM_IMAGE = kernel8-32.img
else
ifeq ("$(ARCH)", "aarch64")
SYSTEM_IMAGE = kernel8.img
endif
endif

