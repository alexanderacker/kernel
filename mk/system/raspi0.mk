# Raspberry Pi Zero
SYSTEM_CPU := arm1176jzf-s
SYSTEM_MEM := 512

SYSTEM_CFLAGS := -mcpu=$(SYSTEM_CPU)
SYSTEM_ARCH := arm
SYSTEM_IMAGE := kernel.img

QEMU_NAME := $(SYSTEM)

.PHONY: system-check
system-check:
